#include "libserver.h"

std::vector<std::shared_ptr<api::server::Entity>> get_minetest_entity_apis()
{
	// Register your api modules here
	return {};
}

std::vector<std::shared_ptr<api::server::Environment>> get_minetest_environment_apis()
{
	// Register your api modules here
	return {};
}

std::vector<std::shared_ptr<api::server::Global>> get_minetest_global_apis()
{
	// Register your api modules here
	return {};
}

std::vector<std::shared_ptr<api::server::Inventory>> get_minetest_inventory_apis()
{
	// Register your api modules here
	return {};
}

std::vector<std::shared_ptr<api::server::Item>> get_minetest_item_apis()
{
	// Register your api modules here
	return {};
}

std::vector<std::shared_ptr<api::server::ModChannels>> get_minetest_modchannels_apis()
{
	// Register your api modules here
	return {};
}

std::vector<std::shared_ptr<api::server::Node>> get_minetest_node_apis()
{
	// Register your api modules here
	return {};
}

std::vector<std::shared_ptr<api::server::NodeMeta>> get_minetest_nodemeta_apis()
{
	// Register your api modules here
	return {};
}

std::vector<std::shared_ptr<api::server::Player>> get_minetest_player_apis()
{
	// Register your api modules here
	return {};
}